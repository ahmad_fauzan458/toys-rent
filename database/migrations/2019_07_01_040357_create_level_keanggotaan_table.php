<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLevelKeanggotaanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('level_keanggotaan', function (Blueprint $table) {
            $table->string('nama_level', 20);
            $table->float('minimum_poin', 8, 2);
            $table->text('deskripsi')->nullable();
            $table->primary('nama_level');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('level_keanggotaan');
        
    }
}
