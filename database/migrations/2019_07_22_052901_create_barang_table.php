<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarangTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('barang', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('nama_item');
            $table->string('warna', 50)->nullable();
            $table->text('url_foto')->nullable();
            $table->text('kondisi');
            $table->integer('lama_penggunaan')->nullable();
            $table->string('no_ktp_pemilik', 20);

            $table->primary('id');
            $table->foreign('nama_item')->references('nama')->on('item')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('no_ktp_pemilik')->references('no_ktp')->on('anggota')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barang');
    }
}
