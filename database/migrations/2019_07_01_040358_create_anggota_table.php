<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnggotaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('anggota', function (Blueprint $table) {
            $table->string('no_ktp', 20);
            $table->integer('poin');
            $table->string('level', 20)->nullable();
            $table->timestamps();
            $table->primary('no_ktp');
            $table->foreign('no_ktp')->references('no_ktp')->on('users')->onDelete('cascade');
            $table->foreign('level')->references('nama_level')->on('level_keanggotaan')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('anggota');
    }
}
