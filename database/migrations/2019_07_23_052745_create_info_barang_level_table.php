<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInfoBarangLevelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('info_barang_level', function (Blueprint $table) {
            $table->uuid('id_barang');
            $table->string('nama_level', 20);
            $table->float('harga_sewa', 8, 2);
            $table->float('porsi_royalti', 8, 2);
            $table->timestamps();

            $table->primary(['id_barang', 'nama_level']);
            $table->foreign('id_barang')->references('id')->on('barang')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('nama_level')->references('nama_level')->on('level_keanggotaan')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('info_barang_level');
    }
}
