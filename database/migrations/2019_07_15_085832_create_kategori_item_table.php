<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKategoriItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kategori_item', function (Blueprint $table) {
            $table->string('nama_item');
            $table->string('nama_kategori');
            $table->timestamps();

            $table->primary(['nama_item','nama_kategori']);
            $table->foreign('nama_item')->references('nama')->on('item')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('nama_kategori')->references('nama')->on('kategori')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kategori_item');
    }
}
