<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlamatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alamat', function (Blueprint $table) {
            $table->string('no_ktp_anggota', 20);
            $table->string('nama');
            $table->string('jalan');
            $table->integer('nomor');
            $table->string('kota');
            $table->string('kode_pos');
            $table->timestamps();
            $table->primary(['nama', 'no_ktp_anggota']);
            $table->foreign('no_ktp_anggota')->references('no_ktp')->on('anggota')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alamat');
    }
}
