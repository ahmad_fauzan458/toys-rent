function init_button(){
	$('.delete-item').click(function(e) {
    	e.preventDefault();
    	show_delete_modal($(this).val());
	});

	$('#confirm-delete').click(function(e) {
    	e.preventDefault();
		  delete_button_action($(this).val());
	});

	$('.view-goods').click(function(e) {
      e.preventDefault();
      window.location.href = "/item/" + $(this).val() + '/barang/';
	});

  $('.add-goods').click(function(e) {
      e.preventDefault();
      window.location.href = ("/item/" + $(this).val() + '/barang/create/');
  });
}

$(document).ready(function(){
	init_button()
	$('#daftar-item-table').DataTable();
});

function delete_button_action(value) {
    $.ajax({
      url: "/item/" + value + "/",
      type: "DELETE",
      data: {
        "_token": $('meta[name="csrf-token"]').attr('content')
      },
      success: function (result) {
		    window.location.reload();      	
      }
    });
}

function show_delete_modal(value) {
	$('#modal-delete-body').text("Anda yakin ingin menghapus item " + value + "?")
	$('#modal-delete').modal('show');
	$('#confirm-delete').val(value);
}