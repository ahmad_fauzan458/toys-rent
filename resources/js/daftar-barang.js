function init_button(){
	$('.delete-barang').click(function(e) {
    	e.preventDefault();
    	show_delete_modal($(this).val());
	});

	$('#confirm-delete').click(function(e) {
    	e.preventDefault();
		  delete_button_action($(this).val());
	});
  
  $('#pesan-barang').click(function(e) {
      e.preventDefault();
      alert(marked_values());
  });
}

$(document).ready(function(){
	init_button()
	$('#daftar-barang-table').DataTable();
});

function delete_button_action(value) {
    $.ajax({
      url: window.location.href + "/" + value + "/",
      type: "DELETE",
      data: {
        "_token": $('meta[name="csrf-token"]').attr('content')
      },
      success: function (result) {
		    window.location.reload();      	
      }
    });
}

function show_delete_modal(value) {
	$('#modal-delete-body').text("Anda yakin ingin menghapus barang dengan id " + value + " ?")
	$('#modal-delete').modal('show');
	$('#confirm-delete').val(value);
}

function marked_values() {
  var values = [];

  $('.mark-barang').each (function() {
    if($(this).prop('checked')) {
      values.push($(this).val());
    }
  });

  return values;
}