@extends('layouts.app')

@section('title')
  Item
@endsection

@section('script')
  <script src="{{ asset('js/daftar-item.js') }}"></script>

  <script
    src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"
    integrity="sha384-rgWRqC0OFPisxlUvl332tiM/qmaNxnlY46eksSZD84t+s2vZlqGeHrncwIRX7CGp"
    crossorigin="anonymous"
  ></script>
  <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"
          integrity="sha384-uiSTMvD1kcI19sAHJDVf68medP9HA2E2PzGis9Efmfsdb8p9+mvbQNgFhzii1MEX"
          crossorigin="anonymous">
  </script>
@endsection

@section('style')
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
@endsection

@section('content') 
  <h1 class="my-4">Daftar Item</h1>
  <div id="daftar-item" class="table-responsive">
    <table id="daftar-item-table" class="table text-center">
      <thead class="thead-dark">
        <tr>
          <th scope="col">Nomor</th>
          <th scope="col">Nama Item</th>
          <th scope="col">Kategori</th>
          <th scope="col">Action</th>
        </tr>
      </thead>
      <tbody>
        @foreach($daftar_item as $item)
        <tr>
          <th scope="row">{{ $loop->iteration }}</th>
          <td>{{ $item['nama_item'] }}</td>
          <td>{{ $item['kategori_item'] }}</td>
          <td>
            <div class="btn-group" role="group" aria-label="Basic example">
              <button type="submit" class="view-goods btn btn-primary" value="{{ $item['nama_item'] }}">Lihat barang-barang</button>
              @if(Session::get('isAdmin'))
              <meta name="csrf-token" content="{{ csrf_token() }}">
            
              <a class="btn btn-primary" href="{{ route('item.edit', [$item['nama_item']]) }}">Update</a>
              <button type="button" class="delete-item btn btn-primary" value="{{ $item['nama_item'] }}">Delete</button>
              <button type="button" class="add-goods btn btn-primary" value="{{ $item['nama_item'] }}">Tambah Barang</button>
              @endif
            </div>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    <div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="modal-delete-title">Delete</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div id="modal-delete-body" class="modal-body"> 
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id="confirm-delete">Confirm</button>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection