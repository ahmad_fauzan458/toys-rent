@extends('item.form')

@section('form-title')
	Form Update Item
@endsection

@section('form-header')
    <form id="update-item-form" action="{{ route('item.update', $item->nama) }}" method="POST">
    {{ method_field('PATCH')}}
@endsection