@extends('layouts.app')

@section('content') 
    <div class="row justify-content-center">
        <div class="col-8">
            <h1 class="display-4 text-center">@yield("form-title")</h1>
            <div class="info-form">
              @yield("form-header")
                @csrf
                <div class="form-group row">
                  <label for="nama" class="col-2 col-form-label">Nama</label>
                  <div class="col-10">
                    <input type="text" class="form-control" id="nama" name="nama" value="{{ isset($item->nama) ? old('nama',$item->nama) : old('nama') }}" required>
                  </div>
                  @if ($errors->has("nama"))
                    <div class="col-10 ml-auto" style="color:red">
                      {{$errors->first("nama")}}
                    </div>
                  @endif
                </div>
                <div class="form-group row">
                  <label for="deskripsi" class="col-2 col-form-label">Deskripsi</label>
                  <div class="col-10">
                    <input type="text" class="form-control" id="deskripsi" name="deskripsi" value="{{ isset($item->deskripsi) ? old('deskripsi',$item->deskripsi) : old('deskripsi') }}">
                  </div>
                  @if ($errors->has("deskripsi"))
                    <div class="col-10 ml-auto" style="color:red">
                      {{$errors->first("deskripsi")}}
                    </div>
                  @endif  
                </div>
                <div class="form-group row">
                  <label class="col-2 col-form-label">Rentang Usia</label>
                  <div class="col-4">
                    <input type="number" class="form-control" id="usia-dari" name="usia_dari" placeholder="Usia minimal" value="{{ isset($item->usia_dari) ? old('usia_dari',$item->usia_dari) : old('usia_dari') }}" required>
                  </div>
                  <div class="col-1 text-center">
                    <p> - </p>
                  </div>
                  <div class="col-4">
                    <input type="number" class="form-control" id="usia-sampai" name="usia_sampai" placeholder="Usia maksimal" value="{{ isset($item->usia_sampai) ? old('usia_sampai',$item->usia_sampai) : old('usia_sampai') }}" required>
                  </div>
                  @if ($errors->has("usia_dari"))
                    <div class="col-10 ml-auto" style="color:red">
                      {{$errors->first("usia_dari")}}
                    </div>
                  @endif
                  @if ($errors->has("usia_sampai"))
                    <div class="col-10 ml-auto" style="color:red">
                      {{$errors->first("usia_sampai")}}
                    </div>
                  @endif
                </div>
                <div class="form-group row">
                  <label for="bahan" class="col-2 col-form-label">Bahan</label>
                  <div class="col-10">
                    <input type="text" class="form-control" id="bahan"  name="bahan" value="{{ isset($item->bahan) ? old('bahan',$item->bahan) : old('bahan') }}">
                  </div>
                  @if ($errors->has("bahan"))
                    <div class="col-10 ml-auto" style="color:red">
                      {{$errors->first("bahan")}}
                    </div>
                  @endif
                </div>
                <div class="form-group row">
                  <label class="col-2 col-form-label">Kategori</label>
                  <div class="col-10">
                    Gunakan ctrl + click (windows) atau command + click (mac) untuk memilih lebih dari satu kategori
                    <select multiple name="kategori[]" class="custom-select" id="kategori">
                      @foreach($daftar_kategori as $kategori)
                      <option>{{ $kategori->nama }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                @if ($errors->has("kategori"))
                  <div class="col-10 ml-auto" style="color:red">
                      <label>{{$errors->first("kategori")}}</label>
                  </div>
                @endif
                <div class="form-group row">
                  <div class="col-12 text-center">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </div>
              </form> 
            </div>
            <br>
        </div>
    </div>
@endsection
