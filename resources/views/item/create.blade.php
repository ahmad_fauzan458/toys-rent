@extends('item.form')

@section('form-title')
	Form Create Item
@endsection

@section('form-header')
    <form id="create-item-form" action="{{ route('item.store') }}" method="POST">
@endsection