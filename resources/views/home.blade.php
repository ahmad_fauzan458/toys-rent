@extends('layouts.app')

@section('title')
  Home
@endsection

@section('content')
<table class="table">
  <tbody>
    <tr>
      <th scope="row">No Ktp</th>
      <td>{{ $user->no_ktp }}</td>
    </tr>
    <tr>
      <th scope="row">Nama Lengkap</th>
      <td>{{ $user->nama }}</td>
    </tr>
    <tr>
      <th scope="row">Email</th>
      <td>{{ $user->email }}</td>
    </tr>
    <tr>
      <th scope="row">Tanggal Lahir</th>
      <td>{{ $user->tanggal_lahir }}</td>
    </tr>
    <tr>
      <th scope="row">Nomor Telepon</th>
      <td>{{ $user->no_telp }}</td>
    </tr>
    @if(Session::get('isAnggota'))
      @foreach($daftar_alamat as $alamat)
      <tr>
        <th scope="row">Alamat {{ $alamat->nama }}</th>
        <td>Jalan {{ $alamat->jalan }}, No.{{ $alamat->nomor }} Kota {{ $alamat->kota }}, {{ $alamat->kode_pos }}</td>
      </tr>
      @endforeach
      <tr>
        <th scope="row">Poin</th>
        <td>{{ $anggota->poin }}</td>
      </tr>
      <tr>
        <th scope="row">Level</th>
        <td>{{ $anggota->level }}</td>
      </tr>
    @endif
  </tbody>
</table>
@endsection
