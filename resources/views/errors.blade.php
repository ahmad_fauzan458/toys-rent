@if ($errors->any())
	<div class="form-group row text-center">
	  <div class="col-12 alert-danger">
	    @foreach ($errors->all() as $error)
	        <div>{{ $error }}</div>
	    @endforeach
	  </div>
	</div>
@endif