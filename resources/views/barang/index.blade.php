@extends('layouts.app')

@section('title')
  Daftar Barang
@endsection

@section('script')
  <script src="{{ asset('js/daftar-barang.js') }}"></script>

  <script
    src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"
    integrity="sha384-rgWRqC0OFPisxlUvl332tiM/qmaNxnlY46eksSZD84t+s2vZlqGeHrncwIRX7CGp"
    crossorigin="anonymous"
  ></script>
  <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"
          integrity="sha384-uiSTMvD1kcI19sAHJDVf68medP9HA2E2PzGis9Efmfsdb8p9+mvbQNgFhzii1MEX"
          crossorigin="anonymous">
  </script>
@endsection

@section('style')
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
@endsection

@section('content') 
  <h1 class="my-4 text-center">Daftar barang</h1>
  <h4 class="my-4 text-center">Item: {{$nama_item}}</h4>
  <div id="daftar-barang" class="table-responsive">
    <table id="daftar-barang-table" class="table text-center">
      <thead class="thead-dark">
        <tr>
          <th scope="col">ID Barang</th>
          <th scope="col">Warna</th>
          <th scope="col">Kondisi</th>
          <th scope="col">Action</th>
          @if(Session::get('isAnggota'))
          <th scope="col">Tandai</th>
          @endif
        </tr>
      </thead>
      <tbody>
        @foreach($daftar_barang as $barang)
        <tr>
          <td>{{ $barang->id }}</td>
          <td>{{ $barang->warna }}</td>
          <td>{{ $barang->kondisi }}</td>
          <td>
            <div class="btn-group" role="group" aria-label="Basic example">
              <a class="btn btn-primary" href="{{ route('item.barang.show', [$nama_item, $barang->id]) }}">Detail</a>

              @if(Session::get('isAdmin'))

              <meta name="csrf-token" content="{{ csrf_token() }}">
              <a class="btn btn-primary" href="{{ route('item.barang.edit', [$nama_item, $barang->id]) }}">Update</a>
              <button type="button" class="delete-barang btn btn-primary" value="{{ $barang->id }}">Delete</button>

              @endif

            </div>
          </td>

          @if(Session::get('isAnggota'))
          <td>
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="mark-barang custom-control-input" id="checkbox-{{ $barang->id }}" value="{{ $barang->id }}">
              <label class="custom-control-label" for="checkbox-{{ $barang->id }}"></label>
            </div>
          </td>
          @endif

        </tr>
        @endforeach
      </tbody>
    </table>
    <div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="modal-delete-title">Delete</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div id="modal-delete-body" class="modal-body"> 
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id="confirm-delete">Confirm</button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <button type="submit" id="pesan-barang" class="col-12 btn btn-primary">Pesan</button>
@endsection