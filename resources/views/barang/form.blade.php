@extends('layouts.app')

@section('content') 
    <div class="row justify-content-center">
        <div class="col-10">
            <h1 class="display-4 text-center">@yield("form-title")</h1>
            <div class="info-form">
              @yield("form-header")
                @csrf
                <label for="nama_item" class="col-form-label">Nama Item</label>
                <div class="form-group row">
                  <div class="col-12">
                    <input type="text" class="form-control" id="nama_item" name="nama_item" value="{{ $nama_item }}" readonly>
                  </div>
                  @if ($errors->has("nama_item"))
                    <div class="col-12 ml-auto" style="color:red">
                      {{$errors->first("nama_item")}}
                    </div>
                  @endif
                </div>
                <label for="warna" class="col-form-label">Warna
                  <span class="text-muted">(Opsional)</span>
                </label>
                <div class="form-group row">
                  <div class="col-12">
                    <input type="text" class="form-control" id="warna" name="warna" value="{{ isset($barang->warna) ? old('warna',$barang->warna) : old('warna') }}">
                  </div>
                  @if ($errors->has("warna"))
                    <div class="col-12 ml-auto" style="color:red">
                      {{$errors->first("warna")}}
                    </div>
                  @endif  
                </div>
                <label for="url_foto" class="col-form-label">URL Foto
                  <span class="text-muted">(Opsional)</span>
                </label>
                <div class="form-group row">
                  <div class="col-12">
                    <input type="text" class="form-control" id="url_foto" name="url_foto" value="{{ isset($barang->url_foto) ? old('url_foto',$barang->url_foto) : old('url_foto') }}">
                  </div>
                  @if ($errors->has("url_foto"))
                    <div class="col-12 ml-auto" style="color:red">
                      {{$errors->first("url_foto")}}
                    </div>
                  @endif  
                </div>
                <label for="kondisi" class="col-form-label">Kondisi</label>
                <div class="form-group row">
                  <div class="col-12">
                    <input type="text" class="form-control" id="kondisi" name="kondisi" value="{{ isset($barang->kondisi) ? old('kondisi',$barang->kondisi) : old('kondisi') }}" required>
                  </div>
                  @if ($errors->has("kondisi"))
                    <div class="col-12 ml-auto" style="color:red">
                      {{$errors->first("kondisi")}}
                    </div>
                  @endif  
                </div>
                <label for="lama_penggunaan" class="col-form-label">Lama Penggunaan
                  <span class="text-muted">(Opsional)</span>
                </label>
                <div class="form-group row">
                  <div class="col-12 input-group">
                    <input type="number" class="form-control" id="lama_penggunaan" name="lama_penggunaan" value="{{ isset($barang->lama_penggunaan) ? old('lama_penggunaan',$barang->lama_penggunaan) : old('lama_penggunaan') }}">
                    <div class="input-group-append">
                        <div class="input-group-text">Bulan</div>
                    </div>
                  </div>
                  @if ($errors->has("lama_penggunaan"))
                    <div class="col-12 ml-auto" style="color:red">
                      {{$errors->first("lama_penggunaan")}}
                    </div>
                  @endif  
                </div>
                <label for="no_ktp_pemilik" class="col-form-label">Pemilik</label>
                <div class="form-group row">
                  <div class="col-12">
                    <select name="no_ktp_pemilik" class="custom-select" id="no_ktp_pemilik">
                      @foreach($daftar_anggota as $anggota)
                      <option value="{{ $anggota->no_ktp }}">{{ $anggota->nama }}</option>
                      @endforeach
                    </select>
                  </div>
                  @if ($errors->has("no_ktp_pemilik"))
                    <div class="col-12 ml-auto" style="color:red">
                      {{$errors->first("no_ktp_pemilik")}}
                    </div>
                  @endif  
                </div>
                <div class="form-group row mr-auto">
                  <h5 class="col-12 font-weight-bold text-center">Info Barang</h5>
                </div>
                @foreach($daftar_level as $level)
                  <div class="form-group row">
                    <label class="col-12 col-form-label mr-auto font-weight-bold">{{$level->nama_level}}</label>
                    <input type="text" class="form-control d-none" id="nama-level-{{ $loop->iteration }}" name="nama_level_{{ $loop->iteration }}" value="{{$level->nama_level}}" required>
                  </div>
                  <div class="form-group row">
                    <div class="col-6">
                      <label class="col-form-label mr-auto">Porsi Royalti</label>
                      <div class="input-group">
                        <input type="number" step="0.01" class="form-control" id="porsi-royalti-{{ $loop->iteration }}" name="porsi_royalti_{{ $loop->iteration }}" value="{{ isset($daftar_info[$level->nama_level]) ? old('porsi_royalti_'.$loop->iteration , $daftar_info[$level->nama_level]->porsi_royalti) : old('porsi_royalti_'.$loop->iteration) }}" required>
                        <div class="input-group-append">
                          <div class="input-group-text">%</div>
                        </div>
                      </div>
                      @if ($errors->has('porsi_royalti_'.$loop->iteration))
                        <div class="ml-auto" style="color:red">
                          {{$errors->first('porsi_royalti_'.$loop->iteration )}}
                        </div>
                      @endif
                    </div>
                    <div class="col-6">
                      <label class="col-form-label ml-auto">Harga Sewa</label>
                      <div class="input-group">
                        <input type="number" step="0.0001" class="form-control" id="harga-sewa-{{ $loop->iteration }}" name="harga_sewa_{{ $loop->iteration }}" value="{{ isset($daftar_info[$level->nama_level]) ? old('harga_sewa_'.$loop->iteration , $daftar_info[$level->nama_level]->harga_sewa) : old('harga_sewa_'.$loop->iteration) }}" required>
                        <div class="input-group-append">
                          <div class="input-group-text">Rupiah/Minggu</div>
                        </div>
                      </div>
                      @if ($errors->has('harga_sewa_'.$loop->iteration))
                        <div class="ml-auto" style="color:red">
                          {{$errors->first("harga_sewa_".$loop->iteration )}}
                        </div>
                      @endif
                    </div>
                  </div>
                @endforeach
                <div class="form-group row">
                  <div class="col-12 text-center">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </div>
              </form> 
            </div>
        </div>
    </div>
@endsection
