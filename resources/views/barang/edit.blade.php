@extends('barang.form')

@section('form-title')
	Form Update Barang
@endsection

@section('form-header')
    <form id="update-barang-form" action="{{ route('item.barang.update', [$nama_item, $barang->id]) }}" method="POST">
    {{ method_field('PATCH')}}
@endsection