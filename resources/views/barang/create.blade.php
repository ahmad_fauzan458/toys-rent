@extends('barang.form')

@section('form-title')
	Form Create barang
@endsection

@section('form-header')
    <form id="create-barang-form" action="{{ route('item.barang.store', $nama_item) }}" method="POST">
@endsection