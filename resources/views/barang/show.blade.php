@extends('layouts.app')

@section('title')
  Detail Barang
@endsection

@section('content')
<div class="row justify-content-center">
  <div class='col-10'>
    <h3 class="row font-weight-bold justify-content-center">
        ID Barang : {{ $barang->id }}
    </h3>
    <table class="table">
      <tbody>
        <tr>
          <th scope="row">Nama Item</th>
          <td>{{ $barang->nama_item }}</td>
        </tr>
        <tr>
          <th scope="row">Warna</th>
          <td>{{ $barang->warna }}</td>
        </tr>
        <tr>
          <th scope="row">URL Foto</th>
          <td>{{ $barang->url_foto }}</td>
        </tr>
        <tr>
          <th scope="row">Kondisi</th>
          <td>{{ $barang->kondisi }}</td>
        </tr>
        <tr>
          <th scope="row">Lama Penggunaan</th>
          <td>{{ $barang->lama_penggunaan }} bulan</td>
        </tr>
        <tr>
          <th scope="row">Pemilik</th>
          <td>{{ $nama_pemilik }}</td>
        </tr>
      @foreach($daftar_level as $level)
        <tr>
          <th scope="row">Porsi Royalti Anggota {{$level->nama_level}}</th>
          <td>{{ $daftar_info[$level->nama_level]->porsi_royalti }} %</td>
        </tr>
        <tr>
          <th scope="row">Harga Sewa Anggota {{$level->nama_level}}</th>
          <td>{{ $daftar_info[$level->nama_level]->harga_sewa }} Rupiah/Minggu</td>
        </tr>
      @endforeach
      </tbody>
    </table>
  </div>
</div>
@endsection
