@extends('layouts.app')

@section('script')
    <script src="{{ asset('js/form-pengguna.js') }}"></script> 
@endsection

@section('content')
<div id="container" class="container">
    <div class="row justify-content-center">
      <div class="col-8 text-center">
          <button id="anggota-button" class="btn btn-info">Anggota</button>
          <button id="admin-button" class="btn btn-secondary">Admin</button>
      </div>
    </div>  
    <div class="row justify-content-center">
        <div class="col-10">
            <h1 class="display-4 text-center">Form Pendaftaran Pengguna</h1>
            <div class="info-form">
              <form action="{{ route('register') }}" method="POST">
                @csrf
                <div class="form-group row" hidden>
                  <label for="tipe_pengguna" class="col-2 col-form-label">Tipe Pengguna</label>
                  <div class="col-10">
                    <input type="text" class="form-control" id="tipe_pengguna" name="tipe_pengguna">
                  </div>
                </div>
               <div class="form-group row">
                  <label for="email" class="col-2 col-form-label">Email</label>
                  <div class="col-10">
                    <input type="text" class="form-control" id="email" name="email"  value="{{ old('email') }}"required>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="password" class="col-2 col-form-label">Password</label>
                  <div class="col-10">
                    <input type="password" class="form-control" id="password" name="password" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="password-confirm" class="col-2 col-form-label">Konfirmasi Password</label>
                  <div class="col-10">
                    <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="no_ktp" class="col-2 col-form-label">No ktp</label>
                  <div class="col-10">
                    <input type="text" class="form-control" id="no_ktp" name="no_ktp"  value="{{ old('no_ktp') }}" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="nama" class="col-2 col-form-label">Nama Lengkap</label>
                  <div class="col-10">
                    <input type="text" class="form-control" id="nama" name="nama" value="{{ old('nama') }}" required>
                  </div>  
                </div>
                <div class="form-group row">
                  <label for="tanggal_lahir" class="col-2 col-form-label">Tanggal Lahir</label>
                  <div class="col-10">
                    <input type="date" class="form-control" id="tanggal_lahir" name="tanggal_lahir" value="{{ old('tanggal_lahir') }}"required>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="no_telp" class="col-2 col-form-label">Nomor Telepon</label>
                  <div class="col-10">
                    <input type="text" class="form-control" id="no_telp" name="no_telp" value="{{ old('no_telp') }}">
                  </div>
                </div>
                <div id="form-alamat" class="form-group row">
                  <div class="col-2 row">
                    <div class="col-7">
                      <label class="col-form-label">Alamat</label>
                    </div>
                    <div class="col-4">
                      <button type="button" class="btn Font-Size-15 py-0 px-1 my-1 ml-auto" id="add-alamat"><i class="fa fa-plus"></i></button>
                    </div>
                  </div>
                  <div class="col-10 ml-auto form-group row" id="form_alamat_1">
                      <div class="form-group col-2">
                        <input type="text" class="form-control nama_alamat" name="nama_alamat_1" placeholder="Nama" required>
                      </div>
                      <div class="form-group col-3">
                        <input type="text" class="form-control alamat_jalan" name="alamat_jalan_1" placeholder="Jalan" required>
                      </div>
                      <div class="form-group col-2">
                        <input type="text" class="form-control alamat_nomor" name="alamat_nomor_1" placeholder="Nomor" required>
                      </div>
                      <div class="form-group col-2">
                        <input type="text" class="form-control alamat_kota" name="alamat_kota_1" placeholder="Kota" required>
                      </div>
                      <div class="form-group col-2">
                        <input type="text" class="form-control alamat_kode_pos" name="alamat_kode_pos_1" placeholder="Kode Pos" required>
                      </div>
                    </div>
                  </div>
                <div class="form-group row">
                  <div class="col-12 text-center">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </div>
                @include('errors')
              </form>

                <div class="col-10 ml-auto form-group row d-none" id="dummy-alamat">
                      <div class="form-group col-2">
                        <input type="text" class="form-control nama_alamat" name="nama_alamat_" placeholder="Nama" required>
                      </div>
                      <div class="form-group col-3">
                        <input type="text" class="form-control alamat_jalan" name="alamat_jalan_" placeholder="Jalan" required>
                      </div>
                      <div class="form-group col-2">
                        <input type="text" class="form-control alamat_nomor" name="alamat_nomor_" placeholder="Nomor" required>
                      </div>
                      <div class="form-group col-2">
                        <input type="text" class="form-control alamat_kota" name="alamat_kota_" placeholder="Kota" required>
                      </div>
                      <div class="form-group col-2">
                        <input type="text" class="form-control alamat_kode_pos" name="alamat_kode_pos_" placeholder="Kode Pos" required>
                      </div>
                       <div class="col-1 m-auto delete-alamat">
                         <button type="button" class="btn Font-Size-15 py-0 px-1 my-1 ml-auto"><i class="fa fa-minus"></i></button>
                      </div>
                </div>

              </div> 
            </div>
          <br>
        </div>
    </div>
</div>
@endsection
