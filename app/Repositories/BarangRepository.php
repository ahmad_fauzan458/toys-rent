<?php 

namespace App\Repositories;

use App\Models\Barang;
use App\Repositories\Contract\BarangRepositoryInterface;

class BarangRepository extends BaseRepository implements BarangRepositoryInterface
{
    
    public function __construct(Barang $barang)
    {
        parent::__construct($barang);
    }

    public function findById($id) {
        return $this->model()
            ->find($id);
    }

    public function findByItemName($name) {
        return $this->model()
            ->where('nama_item', $name)
            ->get()
            ->sortBy('updated_at');
    }
    
    public function getAll() {
        return $this->model()
            ->get()
            ->sortBy('updated_at');
    }
}


