<?php 

namespace App\Repositories\Contract;

interface AlamatRepositoryInterface
{
    public function findByKtp($no_ktp);
}
