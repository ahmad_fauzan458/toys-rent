<?php 

namespace App\Repositories\Contract;

interface KategoriRepositoryInterface
{
    public function findByName($name);

    public function getAll();
}
