<?php 

namespace App\Repositories\Contract;

interface LevelKeanggotaanRepositoryInterface
{
    public function findByLevel($level);

    public function getAll();

    public function count();
}
