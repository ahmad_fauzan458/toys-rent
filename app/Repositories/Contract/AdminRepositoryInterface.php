<?php 

namespace App\Repositories\Contract;

interface AdminRepositoryInterface
{
    public function findByKtp($no_ktp);
}
