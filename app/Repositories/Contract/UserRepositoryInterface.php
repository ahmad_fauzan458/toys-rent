<?php 

namespace App\Repositories\Contract;

interface UserRepositoryInterface
{
    public function findById($id);
    
    public function findByKtp($no_ktp);
}
