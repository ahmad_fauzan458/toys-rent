<?php 

namespace App\Repositories\Contract;

interface ItemRepositoryInterface
{
    public function findByName($name);

    public function getAll();
}
