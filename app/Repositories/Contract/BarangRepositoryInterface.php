<?php 

namespace App\Repositories\Contract;

interface BarangRepositoryInterface
{
    public function findById($name);

    public function findByItemName($name);
    
    public function getAll();
}
