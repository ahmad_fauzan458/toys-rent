<?php 

namespace App\Repositories\Contract;

interface AnggotaRepositoryInterface
{
    public function findByKtp($no_ktp);

    public function getAll();
}
