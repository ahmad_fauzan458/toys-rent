<?php 

namespace App\Repositories\Contract;

interface KategoriItemRepositoryInterface
{
    public function deleteByItemName($name);
}
