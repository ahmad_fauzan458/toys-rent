<?php 

namespace App\Repositories\Contract;

interface InfoBarangLevelRepositoryInterface
{
    public function findByBarangIdAndLevel($barangId, $level);
}
