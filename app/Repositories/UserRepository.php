<?php 

namespace App\Repositories;

use App\Models\User;
use App\Repositories\Contract\UserRepositoryInterface;

class UserRepository extends BaseRepository implements UserRepositoryInterface
{
    
    public function __construct(User $user)
    {
        parent::__construct($user);
    }

    public function findById($id)
    {
        return $this->model()
            ->findOrFail($id);
    }
    
    public function findByKtp($no_ktp) {
        return $this->model()
            ->where('no_ktp', $no_ktp)
            ->first();
    }
}


