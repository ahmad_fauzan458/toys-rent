<?php 

namespace App\Repositories;

use App\Models\Kategori;
use App\Repositories\Contract\KategoriRepositoryInterface;

class KategoriRepository extends BaseRepository implements KategoriRepositoryInterface
{
    
    public function __construct(Kategori $kategori)
    {
        parent::__construct($kategori);
    }

    public function findByName($name) {
        return $this->model()
            ->where('nama', $name)
            ->first();
    }

    public function getAll() {
        return $this->model()
            ->get();
    }
}


