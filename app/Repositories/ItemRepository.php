<?php 

namespace App\Repositories;

use App\Models\Item;
use App\Repositories\Contract\ItemRepositoryInterface;

class ItemRepository extends BaseRepository implements ItemRepositoryInterface
{
    
    public function __construct(Item $item)
    {
        parent::__construct($item);
    }

    public function findByName($name) {
        return $this->model()
            ->where('nama', $name)
            ->first();
    }

    public function getAll() {
        return $this->model()
            ->get()
            ->sortBy('updated_at');
    }
}


