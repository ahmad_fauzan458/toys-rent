<?php 

namespace App\Repositories;

use App\Models\LevelKeanggotaan;
use App\Repositories\Contract\LevelKeanggotaanRepositoryInterface;

class LevelKeanggotaanRepository extends BaseRepository implements LevelKeanggotaanRepositoryInterface
{
    
    public function __construct(LevelKeanggotaan $levelKeanggotaan)
    {
        parent::__construct($levelKeanggotaan);
    }

    public function findByLevel($level) {
        return $this->model()
            ->where('nama_level', $level)
            ->first();
    }

    public function getAll() {
        return $this->model()
            ->get()
            ->sortBy('minimum_poin');
    }

    public function count() {
        return $this->model()
            ->count();
    }
}


