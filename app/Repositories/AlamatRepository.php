<?php 

namespace App\Repositories;

use App\Models\Alamat;
use App\Repositories\Contract\AlamatRepositoryInterface;

class AlamatRepository extends BaseRepository implements AlamatRepositoryInterface
{
    
    public function __construct(Alamat $user)
    {
        parent::__construct($user);
    }

    public function findByKtp($no_ktp) {
        return $this->model()
            ->where('no_ktp', $no_ktp)
            ->first();
    }
}


