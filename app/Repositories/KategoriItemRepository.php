<?php 

namespace App\Repositories;

use App\Models\KategoriItem;
use App\Repositories\Contract\KategoriItemRepositoryInterface;

class KategoriItemRepository extends BaseRepository implements KategoriItemRepositoryInterface
{
    
    public function __construct(KategoriItem $kategoriItem)
    {
        parent::__construct($kategoriItem);
    }

    public function deleteByItemName($name) {
        return $this->model()
            ->where('nama_item', $name)
            ->delete();
    }
}
