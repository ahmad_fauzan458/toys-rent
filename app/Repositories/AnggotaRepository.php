<?php 

namespace App\Repositories;

use App\Models\Anggota;
use App\Repositories\Contract\AnggotaRepositoryInterface;

class AnggotaRepository extends BaseRepository implements AnggotaRepositoryInterface
{
    
    public function __construct(Anggota $anggota)
    {
        parent::__construct($anggota);
    }

    public function findByKtp($no_ktp) {
        return $this->model()
            ->where('no_ktp', $no_ktp)
            ->first();
    }

    public function getAll() {
        return $this->model()
            ->get()
            ->sortBy('no_ktp');
    }
}


