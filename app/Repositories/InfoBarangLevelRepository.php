<?php 

namespace App\Repositories;

use App\Models\InfoBarangLevel;
use App\Repositories\Contract\InfoBarangLevelRepositoryInterface;

class InfoBarangLevelRepository extends BaseRepository implements InfoBarangLevelRepositoryInterface
{
    
    public function __construct(InfoBarangLevel $infoBarangLevel)
    {
        parent::__construct($infoBarangLevel);
    }

    public function findByBarangIdAndLevel($barangId, $level) {
        return $this->model()
            ->where('id_barang', $barangId)
            ->where('nama_level', $level)
            ->first();
    }
}
