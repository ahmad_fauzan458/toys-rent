<?php 

namespace App\Repositories;

use App\Models\Admin;
use App\Repositories\Contract\AdminRepositoryInterface;

class AdminRepository extends BaseRepository implements AdminRepositoryInterface
{
    
    public function __construct(Admin $admin)
    {
        parent::__construct($admin);
    }

    public function findByKtp($no_ktp) {
        return $this->model()
            ->where('no_ktp', $no_ktp)
            ->first();
    }
}


