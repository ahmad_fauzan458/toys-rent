<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Models\Admin;
use App\Models\Anggota;
use App\Models\Alamat;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\DB;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {   
        $rules = [
            'no_ktp' => ['required', 'numeric', 'unique:users'],
            'nama' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'no_telp' => ['string', 'max:20'],
            'tanggal_lahir' => ['required', 'date'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ];

        if ($data['tipe_pengguna'] === 'Anggota') {

            for ($count = 1; $count <= $this->getNumberOfAlamat($data); $count++){
                $rules['nama_alamat_'.$count] = ['required', 'string'];
                $rules['alamat_jalan_'.$count] = ['required', 'string'];
                $rules['alamat_nomor_'.$count] = ['required', 'numeric'];
                $rules['alamat_kota_'.$count] = ['required', 'string'];
                $rules['alamat_kode_pos_'.$count] = ['required', 'string'];                
            }
        }

        return Validator::make($data, $rules);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {   
        $user;
        DB::transaction(function () use ($data, &$user) {
            $user =  User::create([
                'no_ktp' => $data['no_ktp'],
                'nama' => $data['nama'],
                'email' => $data['email'],
                'no_telp' => array_key_exists('no_telp', $data)?$data['no_telp']:null,
                'tanggal_lahir' => $data['tanggal_lahir'], 
                'password' => Hash::make($data['password']),
            ]);

            if ($data['tipe_pengguna'] === 'Admin') {
                Admin::create([
                    'no_ktp' => $data['no_ktp'],
                ]);

                session(['isAdmin' => true ]);
                session(['isAnggota' =>  false ]);

            } else {

                Anggota::create([
                    'no_ktp' => $data['no_ktp'],
                    'poin' => 0,
                ]);

                for ($count = 1; $count <= $this->getNumberOfAlamat($data); $count++) {
                    Alamat::create([
                        'no_ktp_anggota' => $data['no_ktp'],
                        'nama' => $data['nama_alamat_'.$count],
                        'jalan' => $data['alamat_jalan_'.$count],
                        'nomor' => $data['alamat_nomor_'.$count],
                        'kota' => $data['alamat_kota_'.$count],
                        'kode_pos' => $data['alamat_kode_pos_'.$count],
                    ]);
                }

                session(['isAdmin' => false ]);
                session(['isAnggota' => true ]);
            }
        });
        return $user;
    }

    protected function getNumberOfAlamat(array $data){
        $alamat_data = array_slice($data, 9);
        $sizeOfAlamat = 5;
        return ceil(count($alamat_data)/$sizeOfAlamat);
    }

}
