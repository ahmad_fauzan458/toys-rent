<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Repositories\Contract\AdminRepositoryInterface;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';
    protected $adminRepository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(AdminRepositoryInterface $adminRepository)
    {   
        $this->middleware('guest')->except('logout');
        $this->adminRepository = $adminRepository;
    }

    protected function authenticated(Request $request, $user)
    {
        if($this->adminRepository->findByKtp($user->no_ktp))
        {
            session(['isAdmin' => true ]);
            session(['isAnggota' => false ]);
        } else {
            session(['isAdmin' => false ]);
            session(['isAnggota' => true ]);
        }
    }
}
