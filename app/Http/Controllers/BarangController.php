<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use App\Models\InfoBarangLevel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use App\Repositories\Contract\AnggotaRepositoryInterface;
use App\Repositories\Contract\UserRepositoryInterface;
use App\Repositories\Contract\ItemRepositoryInterface;
use App\Repositories\Contract\LevelKeanggotaanRepositoryInterface;
use App\Repositories\Contract\InfoBarangLevelRepositoryInterface;
use App\Repositories\Contract\BarangRepositoryInterface;
use Illuminate\Support\Facades\Validator;

class BarangController extends Controller
{   
    private $anggotaRepository;

    public function __construct
    (   
        AnggotaRepositoryInterface $anggotaRepository,
        ItemRepositoryInterface $itemRepository,
        LevelKeanggotaanRepositoryInterface $levelKeanggotaanRepository,
        InfoBarangLevelRepositoryInterface $infoBarangLevelRepository,
        BarangRepositoryInterface $barangRepository,
        UserRepositoryInterface $userRepository
    )
    {   
        $this->anggotaRepository = $anggotaRepository;
        $this->userRepository = $userRepository;
        $this->itemRepository = $itemRepository;
        $this->levelKeanggotaanRepository = $levelKeanggotaanRepository;
        $this->infoBarangLevelRepository = $infoBarangLevelRepository;
        $this->barangRepository = $barangRepository;
        $this->middleware('auth')->except(['index','show']);
        $this->middleware('admin')->except(['index','show']);
    }


    public function index($namaItem)
    {
        $daftarBarang = ($this->barangRepository->findByItemName($namaItem))->reverse();

        return View::make('barang.index')->with([
            'daftar_barang' => $daftarBarang,
            'nama_item' => $namaItem,
        ]);
    }

    public function create($namaItem)
    {   
        if (!$this->itemRepository->findByName($namaItem)) {
            return response('Not Found', 404);
        }

        $daftarAnggota = $this->anggotaRepository->getAll();
        $daftarUserAnggota = [];

        foreach ($daftarAnggota as $anggota) {
            array_push($daftarUserAnggota, $anggota->user);
        }

        $daftarLevel = $this->levelKeanggotaanRepository->getAll();

        return View::make('barang.create')->with([
            'nama_item' => $namaItem,
            'daftar_anggota' => $daftarUserAnggota,
            'daftar_level' => $daftarLevel,
        ]);
    }

    public function store(Request $request, $namaItem)
    {
        $request['nama_item'] = $namaItem; 
        $data = $this->validateBarang();

        DB::transaction(function() use ($data) {
            $barang = Barang::create(array_slice($data,0,6));

            for ($count = 1; $count <= $this->getNumberOfInfo($data); $count++) {
                InfoBarangLevel::create([
                    'id_barang' => $barang->id,
                    'nama_level' => $data['nama_level_'.$count],
                    'harga_sewa' => $data['harga_sewa_'.$count],
                    'porsi_royalti' => $data['porsi_royalti_'.$count]*0.01,
                ]);
            }
        });

        return redirect()->route('item.barang.index', $namaItem);
    }

    public function show($namaItem, $idBarang)
    {
        $daftarLevel = $this->levelKeanggotaanRepository->getAll();
        $barang = $this->barangRepository->findById($idBarang);
        $pemilik = $this->userRepository->findByKtp($barang->no_ktp_pemilik);

        $daftarInfo = [];
        foreach ($daftarLevel as $level) {
            $info = $this->infoBarangLevelRepository->findByBarangIdAndLevel($idBarang, $level->nama_level);
            $info->porsi_royalti = $info->porsi_royalti*100;
            $daftarInfo[$level->nama_level] = $info;
        }   

        return View::make('barang.show')->with([
            'barang' => $barang,
            'nama_pemilik' => $pemilik->nama,
            'daftar_info' => $daftarInfo,
            'nama_item' => $namaItem,
            'daftar_level' => $daftarLevel,
        ]);           
    }

    public function edit($namaItem, $idBarang)
    {
        if (!$this->itemRepository->findByName($namaItem)) {
            return response('Not Found', 404);
        }

        $daftarAnggota = $this->anggotaRepository->getAll();
        $daftarUserAnggota = [];

        foreach ($daftarAnggota as $anggota) {
            array_push($daftarUserAnggota, $anggota->user);
        }

        $daftarLevel = $this->levelKeanggotaanRepository->getAll();
        $barang = $this->barangRepository->findById($idBarang);

        $daftarInfo = [];
        foreach ($daftarLevel as $level) {
            $info = $this->infoBarangLevelRepository->findByBarangIdAndLevel($idBarang, $level->nama_level);
            $info->porsi_royalti = $info->porsi_royalti*100;
            $daftarInfo[$level->nama_level] = $info;
        }

        return View::make('barang.edit')->with([
            'barang' => $barang,
            'daftar_info' => $daftarInfo,
            'nama_item' => $namaItem,
            'daftar_anggota' => $daftarUserAnggota,
            'daftar_level' => $daftarLevel,
        ]);
    }

    public function update(Request $request, $namaItem, $idBarang)
    {
        $request['nama_item'] = $namaItem; 
        $data = $this->validateBarang();

        DB::transaction(function() use ($data, $idBarang) {
            for ($count = 1; $count <= $this->getNumberOfInfo($data); $count++) {
                
                $info = $this->infoBarangLevelRepository->findByBarangIdAndLevel($idBarang, $data['nama_level_'.$count]);
                
                if($info) {
                    $info->update([
                        'harga_sewa' => $data['harga_sewa_'.$count],
                        'porsi_royalti' => $data['porsi_royalti_'.$count]*0.01,
                    ]);
                } else {
                    InfoBarangLevel::create([
                        'id_barang' => $idBarang,
                        'nama_level' => $data['nama_level_'.$count],
                        'harga_sewa' => $data['harga_sewa_'.$count],
                        'porsi_royalti' => $data['porsi_royalti_'.$count]*0.01,
                    ]);
                }
            }

            $barang = $this->barangRepository->findById($idBarang);       
            $barang->update(array_slice($data,0,6));
        });

        return redirect()->route('item.barang.index', $namaItem);
    }

    public function destroy($namaItem, $idBarang)
    {
        $this->barangRepository->findById($idBarang)->delete();       
    }

    public function validateBarang()
    {
        $rules = [
            'nama_item' => ['required','string','exists:item,nama'],
            'warna' => ['nullable','string','max:50'],
            'url_foto' => ['nullable'],
            'kondisi' => ['required'],
            'lama_penggunaan' => ['nullable','integer','min:0'],
            'no_ktp_pemilik' => ['required','string','max:20','exists:anggota,no_ktp'],
        ];

        $numberOfLevel = $this->levelKeanggotaanRepository->count();

        for ($i = 1; $i <= $numberOfLevel; $i+=1){
            $rules['nama_level_'.$i] = ['required', 'string', 'exists:level_keanggotaan,nama_level'];
            $rules['harga_sewa_'.$i] = ['required', 'numeric', 'min:0'];
            $rules['porsi_royalti_'.$i] = ['required', 'numeric', 'min:0'];
        }

        return request()->validate($rules);
    }

    protected function getNumberOfInfo(array $data){
        $infoData = array_slice($data, 6);
        $sizeOfInfo = 3;
        return ceil(count($infoData)/$sizeOfInfo);
    }
}
