<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\AnggotaRepository;

class HomeController extends Controller
{

    protected $anggotaRepository;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(AnggotaRepository $anggotaRepository)
    {
        $this->middleware('auth');
        $this->anggotaRepository = $anggotaRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $user = auth()->user($request); 
        $user_data = compact('user');

        if ($request->session()->get('isAnggota'))
        {
            $anggota = $this->anggotaRepository->findByKtp($user->no_ktp);
            $daftar_alamat = $anggota->alamat()->get();
            $user_data = compact('user', 'anggota', 'daftar_alamat');
        }
        
        return view('home')->with($user_data);
    }
}
