<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Models\KategoriItem;
use Illuminate\Http\Request;
use App\Repositories\Contract\ItemRepositoryInterface;
use App\Repositories\Contract\KategoriRepositoryInterface;
use App\Repositories\Contract\KategoriItemRepositoryInterface;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;

class ItemController extends Controller
{   
    private $itemRepository;
    private $kategoriRepository;

    public function __construct
    (   
        ItemRepositoryInterface $itemRepository,
        KategoriRepositoryInterface $kategoriRepository,
        KategoriItemRepositoryInterface $kategoriItemRepository
    )
    {   
        $this->itemRepository = $itemRepository;
        $this->kategoriRepository = $kategoriRepository;
        $this->kategoriItemRepository = $kategoriItemRepository;
        $this->middleware('auth')->except('index');
        $this->middleware('admin')->except('index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $daftarItem = ($this->itemRepository->getAll())->reverse();
        $formattedDaftarItem = [];

        foreach ($daftarItem as $item) {   
            $formattedItem = [];

            $formattedItem['nama_item'] = $item['nama'];
            $formattedItem['kategori_item'] = '';
            for ($count = 0; $count < count($item['kategori']); $count++) {
                $formattedItem['kategori_item'] .= $item['kategori'][$count]['nama']; 
                if ($count !== count($item['kategori']) - 1) {
                    $formattedItem['kategori_item'] .= ', ';
                }
            }

            array_push($formattedDaftarItem, $formattedItem);

        }

        return View::make('item.index')->with([
            'daftar_item' => $formattedDaftarItem,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $daftarKategori = $this->kategoriRepository->getAll();
        return View::make('item.create')->with([
            'daftar_kategori' => $daftarKategori,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedRequest = $this->validateItem();
        $itemData = array_slice($validatedRequest, 0, 5);
        $daftarKategori = $validatedRequest['kategori'];

        DB::transaction(function() use ($itemData, $daftarKategori) {

            Item::create($itemData);

            foreach($daftarKategori as $kategori) {
                KategoriItem::create([
                    'nama_item' => $itemData['nama'],
                    'nama_kategori' => $kategori,
                ]);
            }

            
        });
        return redirect()->route('item.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit(Item $item)
    {   
        $daftarKategori = $this->kategoriRepository->getAll();
        return View::make('item.edit')->with([
            'item' => $item,
            'daftar_kategori' => $daftarKategori,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $namaItem)
    {   
        $validatedRequest = $this->validateItem();
        $itemData = array_slice($validatedRequest, 0, 5);
        $daftarKategori = $validatedRequest['kategori'];

        DB::transaction(function() use ($itemData, $daftarKategori, $namaItem) {
            $this->kategoriItemRepository->deleteByItemName($namaItem);
            
            foreach($daftarKategori as $kategori) {
                KategoriItem::create([
                    'nama_item' => $namaItem,
                    'nama_kategori' => $kategori,
                ]);
            }

            $item = $this->itemRepository->findByName($namaItem);       
            $item->update($itemData);
        });
        return redirect()->route('item.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $item)
    {   
        $item->delete();
    }

    public function validateItem()
    {
        return request()->validate([
            'nama' => 'required|string|unique:item,nama',
            'deskripsi' => 'nullable|string',
            'usia_dari' => 'required|integer|max:'.request('usia_sampai'),
            'usia_sampai' => 'required|integer|min:'.request('usia_dari'),
            'bahan' => 'nullable|string',
            'kategori' => 'required|array',
            'kategori.*' => "exists:kategori,nama",
        ]);
    }
}
