<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Repositories\Contract\AdminRepositoryInterface;

class CheckAdmin
{   
    protected $adminRepository;

    public function __construct(AdminRepositoryInterface $adminRepository)
    {
        $this->adminRepository = $adminRepository;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        $admin = $this->adminRepository->findByKTP(Auth::user()->no_ktp);
        if (!$admin)
        {
            return response('Unauthorized action.', 403);
        }

        return $next($request);
    }
}
