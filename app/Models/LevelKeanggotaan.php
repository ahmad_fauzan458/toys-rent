<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LevelKeanggotaan extends Model
{	
	protected $table = 'level_keanggotaan';

    protected $fillable = [
		'nama_level', 'minimum_poin', 'deskripsi',
	];

	public function anggota()
    {
        return $this->hasMany(Anggota::class, "level", "nama_level");
    }
}
