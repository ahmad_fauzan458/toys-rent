<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $table = 'kategori';
    protected $primaryKey = 'nama';
    public $incrementing = false;

    public function superKategori()
    {
        return $this->belongsTo(Kategori::class, "sub_dari", "nama");
    }

    public function subKategori()
    {
        return $this->hasMany(Kategori::class, "sub_dari", "nama");
    }

    public function item()
    {
    	return $this->belongsToMany(Item::class, "kategori_item", "nama_kategori", "nama_item");
    }
}
