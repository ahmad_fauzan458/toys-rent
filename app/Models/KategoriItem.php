<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class KategoriItem extends Model
{
    protected $table = 'kategori_item';

    protected $fillable = [
        'nama_item',
        'nama_kategori',
    ];

    protected function setKeysForSaveQuery(Builder $query)
    {
        $query
            ->where('nama_kategori', '=', $this->getAttribute('nama_kategori'))
            ->where('nama_item', '=', $this->getAttribute('nama_item'));
        return $query;
    }
}
