<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Anggota extends Model
{	
	protected $table = 'anggota';

    protected $fillable = [
		'no_ktp', 'poin',
	];

	public function user()
    {
        return $this->belongsTo(User::class, "no_ktp", "no_ktp");
    }

    public function level()
    {
        return $this->belongsTo(LevelKeanggotaan::class, "level", "nama_level");
    }

    public function alamat()
    {
        return$this->hasMany(Alamat::class, "no_ktp_anggota", "no_ktp");
    }
}
