<?php

namespace App\Models;

use App\Models\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    use UuidTrait;

    protected $table = 'barang';
    public $incrementing = false;

    protected $fillable = [
        'nama_item',
        'warna',
        'url_foto',
        'kondisi',
        'lama_penggunaan',
        'no_ktp_pemilik',
    ];
}
