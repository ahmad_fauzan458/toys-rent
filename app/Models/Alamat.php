<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Alamat extends Model
{	
	protected $table = 'alamat';

    protected $fillable = [
		'no_ktp_anggota', 'nama', 'jalan', 'nomor', 'kota', 'kode_pos',
	];

	public function anggota()
    {
        return $this->belongsTo(Anggota::class, "no_ktp_anggota", "no_ktp");
    }

    protected function setKeysForSaveQuery(Builder $query)
    {
        $query
            ->where('no_ktp_anggota', '=', $this->getAttribute('no_ktp_anggota'))
            ->where('nama', '=', $this->getAttribute('nama'));
        return $query;
    }
}
