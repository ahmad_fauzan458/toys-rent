<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $table = 'item';
    protected $primaryKey = 'nama';
    public $incrementing = false;

    protected $fillable = [
        'nama',
        'deskripsi',
        'usia_dari',
        'usia_sampai',
        'bahan',
    ];

    public function kategori()
    {
    	return $this->belongsToMany(Kategori::class, "kategori_item", "nama_item", "nama_kategori");
    }
}
