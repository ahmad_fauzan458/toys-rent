<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{	
	protected $table = 'admin';

    protected $fillable = [
		'no_ktp',
	];

	public function user()
    {
        return $this->belongsTo(User::class, "no_ktp", "no_ktp");
    }
}
