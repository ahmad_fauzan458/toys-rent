<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class InfoBarangLevel extends Model
{   
    protected $table = 'info_barang_level';

    protected $fillable = [
        'id_barang', 'nama_level', 'harga_sewa', 'porsi_royalti',
    ];

    protected function setKeysForSaveQuery(Builder $query)
    {
        $query
            ->where('id_barang', '=', $this->getAttribute('id_barang'))
            ->where('nama_level', '=', $this->getAttribute('nama_level'));
        return $query;
    }
}
