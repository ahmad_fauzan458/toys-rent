/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/form-pengguna.js":
/*!***************************************!*\
  !*** ./resources/js/form-pengguna.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var alamat_counter = 1;

function change_form() {
  $('#anggota-button').click(function () {
    $('#anggota-button').removeClass('btn-secondary').addClass('btn-info');
    $('#admin-button').removeClass('btn-info').addClass('btn-secondary');
    $('#form-alamat').css('display', 'flex');
    $('#tipe_pengguna').attr('value', 'Anggota');
    $('.nama_alamat').attr('required', true);
    $('.alamat_jalan').attr('required', true);
    $('.alamat_nomor').attr('required', true);
    $('.alamat_kota').attr('required', true);
    $('.alamat_kode_pos').attr('required', true);
  });
  $('#admin-button').click(function () {
    $('#anggota-button').removeClass('btn-info').addClass('btn-secondary');
    $('#admin-button').removeClass('btn-secondary').addClass('btn-info');
    $('#form-alamat').css('display', 'none');
    $('#tipe_pengguna').attr('value', 'Admin');
    $('.nama_alamat').prop('required', false);
    $('.alamat_jalan').prop('required', false);
    $('.alamat_nomor').prop('required', false);
    $('.alamat_kota').prop('required', false);
    $('.alamat_kode_pos').prop('required', false);
  });
}

function modify_alamat() {
  $('#add-alamat').click(function () {
    alamat_counter += 1;
    var $alamat = $('#dummy-alamat').clone();
    var inputs = $alamat.find('input');
    $.each(inputs, function (index, elem) {
      var element = $(elem);
      var name = element.prop('name');
      name += alamat_counter;
      element.prop('name', name);
    });
    $alamat.attr('id', 'form_alamat_' + alamat_counter);
    $alamat.removeClass("d-none").appendTo('#form-alamat');
  });
  $("#form-alamat").delegate(".delete-alamat", "click", function () {
    alamat_counter -= 1;
    $(this).parent().remove();
  });
}

$(document).ready(function () {
  $('#tipe_pengguna').attr('value', 'Anggota');
  change_form();
  modify_alamat();
});

/***/ }),

/***/ 1:
/*!*********************************************!*\
  !*** multi ./resources/js/form-pengguna.js ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/ahmad_fauzan458/toys-rent/resources/js/form-pengguna.js */"./resources/js/form-pengguna.js");


/***/ })

/******/ });